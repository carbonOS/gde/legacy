# Legacy

This repo has some old remains of carbonSHELL; parts of repos that haven't been
ported to the new Graphite Desktop Environment repos, or just code that is
unused but we need to keep around for whatever reason.

Hopefully, we'll be able to delete this one day
