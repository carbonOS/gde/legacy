/* plugin.cpp
 *
 * Copyright 2019 Adrian Vovk <adrianvovk@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "overview.hpp"

void Overview::init(wayfire_config *config) {
    grab_interface->name = "cSH-overview";
    grab_interface->capabilities = wf::CAPABILITY_MANAGE_COMPOSITOR;

    // Settings
    auto section = config->get_section("overview");
    center_opt = section->get_option("center", "1");
    workspace_width = section->get_option("workspace_width", "250");
    auto duration_opt = section->get_option("anim_duration", "500");
    bg_dim_amount = section->get_option("bg_dim", "0.6");
    active_color = section->get_option("active_color", "0.08 0.32 0.62 1"); // Adwaita Blue

    // General callbacks
    damage = [=] () { output->render->damage_whole(); };
    renderer = [=] (const wf_framebuffer& fb) { render_output(fb); };
    reflow_cb = [=] (wf::signal_data_t *data) {
        reflow_windows();
        reflow_workspaces();
    };

    // Animation timers
    anim_duration = wf_duration{duration_opt, wf_animation::circle};
    flow_duration = wf_duration{duration_opt, wf_animation::circle};
    ws_flow_duration = wf_duration{duration_opt, wf_animation::circle};

    // Activation
    auto activation_binding = section->get_option("toggle", "KEY_SCALE | <ctrl> <alt> KEY_TAB | pinch in 5");
    toggle_cb = [=] (wf_activator_source, uint32_t) {
        if (is_active() && !closing)
            close();
        else
            activate();
    };
    output->add_activator(activation_binding, &toggle_cb);

    // Input handling
    grab_interface->callbacks.keyboard.key = [=] (uint32_t key, uint32_t state) {
        if (key == KEY_ESC && state == WLR_KEY_PRESSED) // Close on escape
            close();
    };
    grab_interface->callbacks.pointer.button = [=] (uint32_t btn, uint32_t state) {
        GetTuple(x, y, output->get_cursor_position());

        if (btn == BTN_LEFT && state == WLR_BUTTON_RELEASED) {
            bool status = windows_handle_click(x, y);
            //if (!status) status = workspace_handle_click(x, y);
            if (!status) close();
        }
    };
    grab_interface->callbacks.touch.down = [=] (int32_t id, int32_t x, int32_t y) {
        touch_x = x;
        touch_y = y;
    };
    grab_interface->callbacks.touch.up = [=] (int32_t id) {
        if (id > 0) return;
        bool status = windows_handle_click(touch_x, touch_y);
        //if (!status) status = workspace_handle_click(touch_x, touch_y);
        if (!status) close();
    };
    grab_interface->callbacks.cancel = [=] () { deactivate(); };
}

bool Overview::activate()  { // Open the overview with smooth animation
    if (is_active() || !output->activate_plugin(grab_interface))
        return false;
    wf::get_core().set_cursor("left_ptr");
    grab_interface->grab();
    closing = false;

    // Init renderer
    output->render->add_effect(&damage, wf::OUTPUT_EFFECT_PRE);
    output->render->set_renderer(renderer);
    output->render->set_redraw_always();

    // Init animations
    panel_slide_transition = wf_transition{0.0, 0.9999}; // NOTE: 0.99 b/c 1.0 moves it off screen
    bg_dim_transition = wf_transition{0, bg_dim_amount->as_double()};
    workspace_slide_transition = wf_transition{0, 1};
    reset_anim(anim_duration);

    // Setup windows and workspaces
    reflow_windows();
    reflow_workspaces(true); // true = tells it to not animate y transition
    output->connect_signal("map-view", &reflow_cb);
    output->connect_signal("unmap-view", &reflow_cb);
    return true;
}

void Overview::render_output(const wf_framebuffer& fb) {
    OpenGL::render_begin(fb);
    OpenGL::clear({0, 0, 0, 1});
    OpenGL::render_end();

    for (auto view : get_bg_views())
        view->render_transformed(fb, fb.get_damage_region());
    render_bg_dim(fb);

    for (auto &window : views) {
        transform_window(window);
        window.view->render_transformed(fb, fb.get_damage_region());
        window.view->pop_transformer(wm_transformer); // TODO: Remove
    }

    if (anim_duration.running()) // Only render shell surfaces when they are animating
        for (auto view : get_overlay_views()) {
            transform_shell_surface(view);
            view->render_transformed(fb, fb.get_damage_region());
            view->pop_transformer(shell_transformer); // TODO: Remove
       }

    render_workspace_scrim(fb);
    for (auto &workspace : workspaces) {
        if (!workspace.stream->running)
            output->render->workspace_stream_start(*workspace.stream);
        else
            output->render->workspace_stream_update(*workspace.stream);
        render_workspace_preview(fb, workspace);
    }

    if (closing && !anim_duration.running()) deactivate();
}

void Overview::close() { // Close the overview with smooth animation
    closing = true;
    for (auto &win : views) {
        win.x = wf_transition{win.curr_x, (double) win.real_x};
        win.y = wf_transition{win.curr_y, (double) win.real_y};
        win.scale = wf_transition{win.curr_scale, 1.0};
    }

    panel_slide_transition = wf_transition{0.99, 0};
    bg_dim_transition = wf_transition{bg_dim_amount->as_double(), 0};
    workspace_slide_transition = wf_transition{1, 0};

    reset_anim(anim_duration);
    reset_anim(flow_duration);
    reset_anim(ws_flow_duration);
}

void Overview::deactivate() { // Close the overview with no animation
	if (!is_active())
	    return; // We have nothing to do here
	grab_interface->ungrab();

	output->disconnect_signal("map-view", &reflow_cb);
	output->disconnect_signal("unmap-view", &reflow_cb);

	output->deactivate_plugin(grab_interface);
	output->render->rem_effect(&damage);
	output->render->set_renderer(nullptr);
	output->render->set_redraw_always(false);

	for (auto &view : output->workspace->get_views_in_layer(wf::ALL_LAYERS)) {
	    view->pop_transformer(wm_transformer);
	    view->pop_transformer(shell_transformer);
	}

	for (auto &win : views)
	    win.view->set_activated(win.view_was_activated);
	views.clear();

	clear_workspaces();
}

void Overview::fini() { // Unload the plugin
    deactivate();
    output->rem_binding(&toggle_cb);
}

DECLARE_WAYFIRE_PLUGIN(Overview);
