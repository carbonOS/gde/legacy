/* shell.cpp
 *
 * Copyright 2019 Adrian Vovk <adrianvovk@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "overview.hpp"

std::vector<wayfire_view> Overview::get_bg_views() {
    auto views = output->workspace->get_views_on_workspace(
        output->workspace->get_current_workspace(), wf::BELOW_LAYERS, false);
    std::reverse(views.begin(), views.end()); // Set the correct z order
    return views;
}

std::vector<wayfire_view> Overview::get_overlay_views() {
    auto views = output->workspace->get_views_on_workspace(
        output->workspace->get_current_workspace(), wf::ABOVE_LAYERS, false);
    std::reverse(views.begin(), views.end()); // Set the correct z order
    return views;
}

void Overview::render_bg_dim(const wf_framebuffer& fb) {
    float bg_dim_amount = anim_duration.progress(bg_dim_transition);
    wf_geometry screen = output->get_relative_geometry();
    render_box(fb, screen, screen, {0, 0, 0, bg_dim_amount});
}

void Overview::transform_shell_surface(wayfire_view view) {
    wf_geometry output_geom = output->get_relative_geometry();
    wf_geometry window = view->get_wm_geometry();
    float percent = anim_duration.progress(panel_slide_transition);

    double ld = window.x, rd = output_geom.width - (window.x + window.width);
    double td = window.y, bd = output_geom.height - (window.y + window.height);
    double translation_x = 0, translation_y = 0;
    if (ld < rd)
        translation_x -= ld + window.width;
    else if (rd < ld)
        translation_x += rd + window.width;
    if (bd < td)
        translation_y += bd + window.height;
    else
        translation_y -= td + window.height;

    if (!view->get_transformer(shell_transformer))
        view->add_transformer(std::make_unique<wf_2D_view>(view), shell_transformer);

    auto tr = dynamic_cast<wf_2D_view*>(view->get_transformer(shell_transformer).get());
    tr->translation_x = translation_x * percent;
    tr->translation_y = translation_y * percent;
}
