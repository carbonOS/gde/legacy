/* overview.hpp
 *
 * Copyright 2019 Adrian Vovk <adrianvovk@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef _CSH_OVERVIEW_H
#define _CSH_OVERVIEW_H

#define WAYFIRE_PLUGIN
#include <wayfire/plugin.hpp>
#include <opengl.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <wayfire/render-manager.hpp>
#include <wayfire/core.hpp>
#include <wayfire/view.hpp>
#include <wayfire/view-transform.hpp>
#include <wayfire/workspace-manager.hpp>
#include <wayfire/workspace-stream.hpp>
#include <wayfire/signal-definitions.hpp>
#include <wayfire/animation.hpp>
#include <wayfire/debug.hpp>
#include <linux/input-event-codes.h>
#include <cfloat>

extern "C"
{
#define static
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_matrix.h>
#undef static
}

#define wm_transformer "cSH_overview_wm"
#define shell_transformer "cSH_overview_shell"

#define view_workarea_margin 20

class Overview : public wf::plugin_interface_t {
    bool closing;
    activator_callback toggle_cb;
    wf::signal_callback_t reflow_cb;
    wf::effect_hook_t damage;
    wf::render_hook_t renderer;

    // Settings
    wf_option center_opt;
    wf_option workspace_width;
    wf_option bg_dim_amount;
    wf_option active_color;

    // Animation
    wf_duration anim_duration; // Opening/closing
    wf_duration flow_duration; // Reflowing the overview windows
    wf_duration ws_flow_duration; // Reflowing the workspaces

    wf_transition bg_dim_transition;
    wf_transition panel_slide_transition;
    wf_transition workspace_slide_transition;

    // Interaction
    int touch_x, touch_y;

    inline bool is_active() { // Is the panel open, opening, or closing
        return output->is_plugin_active(grab_interface->name);
    }

    inline void reset_anim(wf_duration& anim) { // Restarts an animation
        if (!anim.running())
            anim.start(0, 1);
        else
            anim.start(anim.progress(), 1);
    }

    std::vector<wayfire_view> get_bg_views();
    std::vector<wayfire_view> get_overlay_views();
    void render_bg_dim(const wf_framebuffer& fb);
    void transform_shell_surface(wayfire_view view);

    struct OverviewWindow {
        wayfire_view view;
        bool view_was_activated;

        wf_transition x;
        wf_transition y;
        wf_transition scale;
        int real_x, real_y;
        double curr_x, curr_y, curr_scale;

        // Only used inside of reflow_windows
        int row = -1, col = -1;
        double candidate_dist = DBL_MAX;
    };
    std::vector<OverviewWindow> views;
    std::vector<wayfire_view> get_wm_views(std::tuple<int, int> ws = {-1, -1});
    void reflow_windows();
    void transform_window(OverviewWindow &win);
    bool windows_handle_click(int x, int y);

    struct OverviewWorkspace {
        wf::workspace_stream_t* stream;
        bool empty = false;
        int v_index; // Fake index used in overview
        int r_index; // Real index used in wayfire

        wf_transition indicator_trans = {0, 4};
        wf_transition x;
        wf_transition y;
        double curr_x, curr_y;
    };
    std::vector<OverviewWorkspace> workspaces;
    void reflow_workspaces(bool opening = false);
    void clear_workspaces();
    void render_box(const wf_framebuffer& fb, wf_geometry screen, wlr_box geometry, wf_color color);
    void render_workspace_scrim(const wf_framebuffer& fb);
    void render_workspace_preview(const wf_framebuffer& fb, OverviewWorkspace& ws);

    bool activate();
    void render_output(const wf_framebuffer& fb);
    void close();
    void deactivate();

    public:
    void init(wayfire_config *config) override;
    void fini() override;
};
#endif
