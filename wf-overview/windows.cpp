/* windows.cpp
 *
 * Copyright 2019 Adrian Vovk <adrianvovk@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "overview.hpp"

std::vector<wayfire_view> Overview::get_wm_views(std::tuple<int, int> ws) {
    if (std::get<0>(ws) == -1) ws = output->workspace->get_current_workspace();
    auto all_views = output->workspace->get_views_on_workspace(
        ws, wf::WM_LAYERS | wf::LAYER_MINIMIZED, true);
    std::vector<wayfire_view> mapped_views;
    for (auto view : all_views) {
        if (view->is_mapped()) mapped_views.push_back(view);
    }
    std::reverse(mapped_views.begin(), mapped_views.end()); // Set the correct z order
    return mapped_views;
}

void Overview::transform_window(OverviewWindow &win) {
    if (!win.view->get_transformer(wm_transformer))
        win.view->add_transformer(std::make_unique<wf_2D_view>(win.view), wm_transformer);

    if (!flow_duration.running()) { // Once the animation is done, update the current position
        win.curr_x = win.x.start = win.x.end;
        win.curr_y = win.y.start = win.y.end;
        win.curr_scale = win.scale.start = win.scale.end;
    }// else {
        auto tr = dynamic_cast<wf_2D_view*>(win.view->get_transformer(wm_transformer).get());
        auto geom = win.view->get_wm_geometry();

        tr->scale_x = tr->scale_y = win.curr_scale = flow_duration.progress(win.scale);
        win.curr_x = flow_duration.progress(win.x);
        tr->translation_x = win.curr_x - win.real_x - (geom.width * (1 - tr->scale_x) / 2);
        win.curr_y = flow_duration.progress(win.y);
        tr->translation_y = win.curr_y - win.real_y - (geom.height * (1 - tr->scale_y) / 2);
    //}
}

void Overview::reflow_windows() {
    // Setup the list of views
    std::vector<OverviewWindow> new_views;
    for (auto view : get_wm_views()) {
        bool found = false;
        for (auto win : views) // If this view existed before, copy it
            if (win.view == view) {
                new_views.push_back(win);
                found = true;
                break;
            }
        if (found) continue; // We don't need to make a new one

        auto geometry = view->get_wm_geometry();
        OverviewWindow win;
        win.view = view;
        win.view_was_activated = view->activated;
        view->set_activated(true); // Make the window appear focused
        win.curr_x = win.real_x = win.x.start = win.x.end = geometry.x;
        win.curr_y = win.real_y = win.y.start = win.y.end = geometry.y;
        win.curr_scale = win.scale.start = win.scale.end = 1.0;
        new_views.push_back(win);
    }
    views.clear(); // Delete the old windows
    views = new_views; // Use the new ones
    if (views.size() == 0) return; // We have nothing to do here

    wf_geometry output_geom = output->get_relative_geometry();
    wf_geometry workarea;
    workarea.x = workarea.y = view_workarea_margin;
    workarea.width = output_geom.width - workarea.x - (view_workarea_margin + workspace_width->as_double());
    workarea.height = output_geom.height - workarea.y - view_workarea_margin;
    double num_windows = (double) views.size();
    int cols = (int) ceil(sqrt(num_windows));
    int rows = (int) ceil(num_windows / cols);
    double slot_width = workarea.width / cols;
    double slot_height = workarea.height / rows;

    // Assign a row,col to each window
    std::vector<OverviewWindow*> to_process;
    for (auto &win : views)
        to_process.push_back(&win);
    while (to_process.size() != 0) {
        auto win = to_process.back();
        to_process.pop_back();

        win->row = -1;
        win->col = -1;
        win->candidate_dist = DBL_MAX;

        for (int row = 0; row < rows; row++)
            for (int col = 0; col < cols; col++) {
                // Calculate the distance to the slot
                double x_dist = win->real_x - (slot_width * col);
                double y_dist = win->real_y - (slot_height * row);
                double dist = sqrt(x_dist * x_dist + y_dist * y_dist);
                if (dist > win->candidate_dist) continue; // This slot isn't the closest

                // Check if slot is occupied by a better window
                bool accept = true;
                for (auto &win : views) {
                    if (win.row == row && win.col == col) {
                        if (win.candidate_dist > dist) {
                            to_process.push_back(&win); // Kick this window out
                        } else accept = false;
                        break; // We found the window we wanted. Stop looking
                    }
                }
                if (!accept) continue; // The slot is occupied. Find a new one

                // This slot is perfect. Use it.
                win->candidate_dist = dist;
                win->row = row;
                win->col = col;
        }
    }

    // Used in centering logic below
    bool rowcount[rows][cols];
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < cols; j++)
            rowcount[i][j] = false;

    // Position each window in its slot
    for (auto &win : views) {
        auto rect = win.view->get_wm_geometry();
        auto x_scale = slot_width / rect.width;
        auto y_scale = slot_height / rect.height;
        double scale = win.scale.end = std::min(x_scale, y_scale) * 0.95;
        if (win.scale.end > 1.0) // Don't scale up views
            scale = win.scale.end = 1.0;

        win.x.end =
            workarea.x + (win.col * slot_width) + // Place in slot
            (slot_width / 2) - (rect.width * scale / 2); // Center in slot
        win.y.end =
            workarea.y + (win.row * slot_height) + // Place in slot
            (slot_height / 2) - (rect.height * scale / 2); // Center in slot

        rowcount[win.row][win.col] = true;
    }

    // Center rows if necessary
    if (center_opt->as_int() && rows * cols > num_windows)
        for (auto &win : views) {
            int in_row = 0, before_win = 0;
            for (int i = 0; i < cols; i++)
                if(rowcount[win.row][i]) {
                    in_row++;
                    if (i < win.col) before_win++;
                }

            if (in_row < cols) {
                // Normalize the column number
                win.x.end -= (win.col * slot_width);
                win.col = before_win;

                // Move to the center column
                win.col += floor((cols - in_row) / 2);
                win.x.end += (win.col * slot_width);

                // When there is an odd # of empty cols, shift by 1/2 of a col
                if ((cols - in_row) % 2 != 0)
                    win.x.end += slot_width / 2;
            }
        }

    // Set up animation
    for (auto &win : views) {
        win.x.start = win.curr_x;
        win.y.start = win.curr_y;
        win.scale.start = win.curr_scale;
    }
    reset_anim(flow_duration); // Animate the reflow
}

bool Overview::windows_handle_click(int x, int y) {
    wayfire_view found = nullptr;
    for (auto win = views.begin(); win != views.end(); ++win) {
        auto geom = win->view->get_wm_geometry();
        int width = geom.width * win->curr_scale;
        int height = geom.height * win->curr_scale;
        if (x > win->curr_x && x < win->curr_x + width &&
                y > win->curr_y && y < win->curr_y + height) {
            win->view->focus_request(); // Request focus

            // Bring it to the top (render it last)
            auto bak = *win; // Make a copy of win
            views.erase(win);
            views.insert(views.end(), bak);

            found = bak.view;
        }
    }
    if (found == nullptr) return false;
    for (auto &win : views) // Fix the window focus appearance
        win.view_was_activated = (win.view == found);
    close();
    return true;
}
